FROM golang:1.21.6
WORKDIR /app
COPY . .
RUN GOOS=linux CGO_ENABLED=0 go build -o cloud-run ./cmd/temperature-api
CMD [ "./cloud-run" ]
