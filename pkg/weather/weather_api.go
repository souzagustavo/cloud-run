package weather

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"
)

var (
	ErrWeather = fmt.Errorf("can not found weather info")
)

type WeatherResponse struct {
	Results struct {
		TempCelsius float64 `json:"temp"`
	} `json:"results"`
}

type WeatherApi struct {
	Key string
}

func NewWeatherApi(key string) *WeatherApi {
	return &WeatherApi{
		Key: key,
	}
}

func (w *WeatherApi) GetWeather(ctx context.Context, city, state string, timeout time.Duration) (float64, error) {
	var temperature float64

	//cria contexto com timeout da API
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	//cria requisição com contexto de timeout
	req, err := http.NewRequestWithContext(ctx, "GET", "https://api.hgbrasil.com/weather", nil)
	if err != nil {
		log.Println(err.Error())
		return temperature, ErrWeather
	}
	query := req.URL.Query()
	query.Add("city_name", fmt.Sprintf("%s, %s", city, strings.ToUpper(state)))
	query.Add("key", w.Key)
	req.URL.RawQuery = query.Encode()

	log.Println("Executing GET request to URL: " + req.URL.String())

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}
	//executa requisição
	response, err := client.Do(req)
	if err != nil {
		log.Println(err.Error())
		return temperature, ErrWeather
	}
	//body sempre deve ser fechado
	defer response.Body.Close()

	//atribui status ao ACK
	if response.StatusCode != 200 {
		log.Println("Status != 200")
		return temperature, ErrWeather
	}

	var weatherResponse WeatherResponse
	err = json.NewDecoder(response.Body).Decode(&weatherResponse)
	if err != nil {
		log.Println(err.Error())
		return temperature, ErrWeather
	}
	temperature = weatherResponse.Results.TempCelsius

	return temperature, nil
}
