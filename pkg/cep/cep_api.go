package cep

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
)

var (
	ErrCepPattern         = fmt.Errorf("cep should follow the pattern 99999-888 or 99999888")
	ErrInvalidZipcode     = fmt.Errorf("invalid zipcode")
	ErrCannotFoundZipcode = fmt.Errorf("can not found zipcode")
)

type CepResponse struct {
	City  string `json:"localidade"`
	State string `json:"uf"`
}

type CepApi struct {
	//endereço da API com o CEP formatado como %s. Por exemplo: http://dns.com.br?cep=%s ou http://dns/%s.json
	url string
	//timeout: tempo limite para executar requisição em milisegundos
	timeout time.Duration
}

// construtor para VIA CEP API
func NewViaCepApi(viaCepUrl string, timeout time.Duration) *CepApi {
	return newCepApi(viaCepUrl, timeout)
}

// construtor privado
func newCepApi(url string, timeout time.Duration) *CepApi {
	return &CepApi{
		url:     url,
		timeout: timeout,
	}
}

// retorna cidade associada ao CEP
func (api *CepApi) GetCep(ctx context.Context, cep string) (*CepResponse, error) {
	var cepResponse CepResponse
	cep, err := api.formatCep(cep)
	if err != nil {
		log.Println(err.Error())
		return &cepResponse, ErrInvalidZipcode
	}
	//cria contexto com timeout da API
	ctx, cancel := context.WithTimeout(ctx, api.timeout)
	defer cancel()

	//cria URL da requisição substituindo %s pelo valor do CEP
	urlWithCep := fmt.Sprintf(api.url, cep)

	//cria requisição com contexto de timeout
	req, err := http.NewRequestWithContext(ctx, "GET", urlWithCep, nil)
	if err != nil {
		log.Println(err.Error())
		return &cepResponse, ErrCannotFoundZipcode
	}

	log.Println("Executing GET request to URL: " + urlWithCep)
	//executa requisição
	response, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Println(err.Error())
		return &cepResponse, ErrCannotFoundZipcode
	}
	//body sempre deve ser fechado
	defer response.Body.Close()

	//atribui status ao ACK
	if response.StatusCode != 200 {
		log.Printf("Http request GetCep status=%d, returning ErrCannotFoundZipCode\n", response.StatusCode)
		return &cepResponse, ErrCannotFoundZipcode
	}

	err = json.NewDecoder(response.Body).Decode(&cepResponse)
	if err != nil {
		log.Println(err.Error())
		return &cepResponse, ErrCannotFoundZipcode
	}

	if cepResponse.City == "" {
		log.Printf(`Http request GetCep returned empty 'logradouro' for zipcode=%s, returning ErrCannotFoundZipCode\n`, cep)
		return &cepResponse, ErrCannotFoundZipcode
	}

	return &cepResponse, nil
}

func (api *CepApi) formatCep(cep string) (string, error) {
	//tamanho do cep
	length := len(cep)

	//valida se cep tem tamanho 8 ou 9
	if length != 8 && length != 9 {
		return cep, ErrCepPattern
	}

	if length == 8 {
		//se tamanho for 8, deve possuir apenas digitos
		if _, err := strconv.Atoi(cep); err != nil {
			return cep, ErrCepPattern
		}
		//retornar cep com hífen
		return fmt.Sprintf("%s-%s", cep[:5], cep[5:]), nil
	} else {
		//se tamanho for 9, deve possuir um hífen
		cepSplit := strings.Split(cep, "-")

		//verifica se cep possui digitos a esquerda e a direita do hífen
		if len(cepSplit) != 2 {
			return cep, ErrCepPattern
		}
		for _, cepPart := range cepSplit {
			if _, err := strconv.Atoi(cepPart); err != nil {
				return cep, ErrCepPattern
			}
		}
		return cep, nil
	}
}
