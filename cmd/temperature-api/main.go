package main

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/souzagustavo/cloud-run-temperature/internal/infra/web"
)

func main() {
	weatherHandler := web.NewWebWeatherHandler()

	router := chi.NewMux()
	router.Use(middleware.Logger)
	router.Get("/clima", weatherHandler.GetWeather)

	web.NewWebServer(":8080").Start(router)

}
