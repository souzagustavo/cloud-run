# README

## Funcionamento Server HTTP

* Rota: GET /clima?cep=<INFORMAR_CEP>
    * Arquivo para teste em `api/api.http`

* API de tempo utilizada: https://api.hgbrasil.com/weather
* API de cep utilizada: http://viacep.com.br

* Problema de usar https://weatherapi.com:
    * Só é possível filtrar/buscar o clima de uma cidade brasileira através da API https://www.weatherapi.com/  usando latitude/longitude ou nome da cidade sem estado;
    * http://viacep.com.br não retorna latitude/longitude;
    * Existe mais de uma cidade com mesmo nome. Portanto, a temperatura exibida pode ser de outra cidade com mesmo nome da cidade do cep!

## Executar testes unitários
* Comando a partir da raíz:
```
go test -v ./...
```

## Executar docker-compose ambiente de Desenvolvimento

* Comando para subir aplicação:
```
docker-compose up dev --build
```
* Service URL exposto na porta 8080:
```
http://localhost:8080/clima?cep=*********
```

## Executar docker-compose ambiente de Produção

* Comando para subir aplicação:
```
docker-compose up prod --build
```
* Service URL exposto na porta 8081:
```
http://localhost:8081/clima?cep=*********
```

## Chamada para cloud-run

* Container image URL
```
gcr.io/fullcycle-poc/cloud-run-weather/
```
* Service URL:
```
https://cloud-run-weather-7vxmx3tjwa-uc.a.run.app/clima?cep=*********
```


    

