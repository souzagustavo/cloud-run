package web

import (
	"fmt"
	"log"
	"net/http"
	"strings"
)

type WebServer struct {
	Port string
}

func NewWebServer(port string) *WebServer {
	return &WebServer{Port: port}
}

func (w *WebServer) Start(handler http.Handler) {
	port, _ := strings.CutPrefix(w.Port, ":")
	log.Printf("Initializing web server at port :%s\n", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), handler))
}
