package web

import (
	"encoding/json"
	"net/http"

	"gitlab.com/souzagustavo/cloud-run-temperature/internal/model/dto"
	usecase "gitlab.com/souzagustavo/cloud-run-temperature/internal/use_case"
)

type WebWeatherHandler struct {
}

func NewWebWeatherHandler() *WebWeatherHandler {
	return &WebWeatherHandler{}
}

func (h *WebWeatherHandler) GetWeather(w http.ResponseWriter, r *http.Request) {

	inputDTO := dto.GetTemperatureInput{
		Cep: r.URL.Query().Get("cep"),
	}

	outputDTO := usecase.NewGetTemperature(inputDTO)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(outputDTO.StatusCode)

	err := json.NewEncoder(w).Encode(&outputDTO)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

}
