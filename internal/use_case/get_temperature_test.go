package usecase

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/souzagustavo/cloud-run-temperature/internal/model/dto"
	"gitlab.com/souzagustavo/cloud-run-temperature/pkg/cep"
)

func TestGetTemperature(t *testing.T) {
	t.Run("Cep válido", func(t *testing.T) {
		input := dto.GetTemperatureInput{
			Cep: "88075290",
		}

		output := NewGetTemperature(input)

		assert.GreaterOrEqual(t, output.Celsius, float64(0))
		assert.GreaterOrEqual(t, output.Fahrenheit, float64(0))
		assert.GreaterOrEqual(t, output.Kelvin, float64(0))
		assert.Equal(t, http.StatusOK, output.StatusCode)

	})

	t.Run("Cep inválido", func(t *testing.T) {
		input := dto.GetTemperatureInput{
			Cep: "88075290xxxxx",
		}

		output := NewGetTemperature(input)

		assert.Equal(t, cep.ErrInvalidZipcode.Error(), output.Message)
		assert.Equal(t, http.StatusUnprocessableEntity, output.StatusCode)
	})

	t.Run("Cep não encontrado", func(t *testing.T) {
		input := dto.GetTemperatureInput{
			Cep: "00000000",
		}

		output := NewGetTemperature(input)

		assert.Equal(t, cep.ErrCannotFoundZipcode.Error(), output.Message)
		assert.Equal(t, http.StatusNotFound, output.StatusCode)
	})
}
