package usecase

import (
	"context"
	"errors"
	"log"
	"net/http"
	"time"

	"gitlab.com/souzagustavo/cloud-run-temperature/internal/model/dto"
	"gitlab.com/souzagustavo/cloud-run-temperature/pkg/cep"
	"gitlab.com/souzagustavo/cloud-run-temperature/pkg/weather"
)

func NewGetTemperature(input dto.GetTemperatureInput) dto.GetTemperatureOutput {
	var outputDTO dto.GetTemperatureOutput

	viaCepApi := cep.NewViaCepApi("http://viacep.com.br/ws/%s/json/", 10*time.Second)

	ctx := context.Background()
	cepResponse, err := viaCepApi.GetCep(ctx, input.Cep)
	if err != nil {
		outputDTO.Message = err.Error()
		if errors.Is(err, cep.ErrInvalidZipcode) {
			outputDTO.StatusCode = http.StatusUnprocessableEntity
		} else if errors.Is(err, cep.ErrCannotFoundZipcode) {
			outputDTO.StatusCode = http.StatusNotFound
		} else {
			outputDTO.StatusCode = http.StatusInternalServerError
		}
		return outputDTO
	}

	log.Printf("Cep City Response: %s, %s\n", cepResponse.City, cepResponse.State)

	weatherApi := weather.NewWeatherApi("38de02ec")

	temperatureCelsius, err := weatherApi.GetWeather(ctx, cepResponse.City, cepResponse.State, 10*time.Second)
	if err != nil {
		outputDTO.Message = err.Error()
		outputDTO.StatusCode = http.StatusNotFound
		return outputDTO
	}

	log.Printf("Weather Celsius Temperature Response: %.2fC\n", temperatureCelsius)

	outputDTO.StatusCode = http.StatusOK
	outputDTO.Celsius = temperatureCelsius
	outputDTO.Fahrenheit = (temperatureCelsius * 1.8) + 32
	outputDTO.Kelvin = temperatureCelsius + 273
	return outputDTO
}
