package dto

type HttpOutput struct {
	Message    string `json:"message,omitempty"`
	StatusCode int    `json:"-"`
}
